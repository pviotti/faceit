package it.enchante;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.parser.ParseException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class EnchanteActivity extends Activity {

	private Button btnEnter;

	private ProfileManager prfManager;

	private EditText tName;
	private EditText tSurname;
	private EditText tEmail;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		/*try
		{
			 Scanner s = new Scanner(new File(Environment.getExternalStorageDirectory(),"TokenSegreto.txt"));
			 String TOKEN = s.nextLine();
			 Log.d("EnchanteActivity", "TOKEN:"+TOKEN);
			 ProfileManager prof = ProfileManager.getInstance();
			 prof.TOKEN = TOKEN;
			 startActivity(new Intent(getApplicationContext(),NearMeActivity.class));
		} catch (FileNotFoundException e) {
			Log.d("EnchanteActivity","TOKEN NOT FOUND");
		}*/
		setContentView(R.layout.main);
		
		btnEnter = (Button) findViewById(R.id.submit_btn);
		tName = (EditText) findViewById(R.id.editText1);
		tSurname = (EditText) findViewById(R.id.editText2);
		tEmail = (EditText) findViewById(R.id.editText3);

		prfManager = ProfileManager.getInstance();

		btnEnter.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try {
					if (!isNetworkAvailable()) {
						showToast("Please activate a network connection.", Toast.LENGTH_SHORT);
						return;
					}
					if(tName.getText().toString()=="" || tSurname.getText().toString()=="" || tEmail.getText().toString()=="") {
						showToast("Compile everything please", Toast.LENGTH_SHORT);
						return;
					}
					if (prfManager.doCreateUser(tName.getText().toString(), tSurname.getText().toString(),
							tEmail.getText().toString())) {
						startActivity(new Intent(getApplicationContext(),ProfileActivity.class)); //Starts the personal profile activity
						//startActivity(new Intent(getApplicationContext(),NearMeActivity.class));
						ProfileManager prof = ProfileManager.getInstance();
						prof.UserName=tName.getText().toString();
						prof.UserSurname=tSurname.getText().toString();
						prof.UserEmail=tEmail.getText().toString();
					} 
					else {
						showToast("Could not login!", Toast.LENGTH_SHORT);
					}
				} catch (IOException e) {
					showToast("Exception: " + e.getMessage(), Toast.LENGTH_SHORT);
				} catch (ParseException e) {
					showToast(e.getMessage(), Toast.LENGTH_LONG);
				}
			}
		});
	}

	public void showToast(CharSequence text, int duration) {
		Context context = getApplicationContext();
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	public boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true; 
		return false;
	}
}