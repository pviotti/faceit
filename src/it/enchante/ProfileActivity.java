package it.enchante;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.R.bool;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;


public class ProfileActivity extends Activity {
	ImageButton _photoBtn;
	boolean _hasMadePhoto;
	Bitmap _mImageBitmap;
	
	private EditText fbAccount;
	private EditText skypeAccount;
	private EditText twAccount;
	private EditText lnAccount;
	private EditText tags;

	public void onCreate(Bundle savedInstanceState) {
		_hasMadePhoto= false;
		super.onCreate(savedInstanceState);
		final Ench_wifiActivity wifiAct = new Ench_wifiActivity();
		wifiAct.initWifiManager(this);
		Timer wifiTimer = new Timer("WifiTimer");
		wifiTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				wifiAct.startScan();
			}
		},new Date(),20000);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.profile_activity);
		TextView nameText = (TextView)findViewById(R.id.profile_name);
		TextView surnameText = (TextView)findViewById(R.id.profile_surname);
		TextView emailText = (TextView)findViewById(R.id.profile_email);
		
		fbAccount = (EditText) findViewById(R.id.facebook_account);
		skypeAccount = (EditText) findViewById(R.id.skype_account);
		twAccount = (EditText) findViewById(R.id.twitter_account);
		lnAccount = (EditText) findViewById(R.id.linkedin_account);
		tags = (EditText) findViewById(R.id.tags);
		
		ProfileManager prof = ProfileManager.getInstance();
		nameText.setText(prof.UserName);
		surnameText.setText(prof.UserSurname);
		emailText.setText(prof.UserEmail);
		Button registerBtn = (Button)findViewById(R.id.btn_register2);
		registerBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(_hasMadePhoto) {
					//DO YOUR FUCKING UPLOAD
					ProfileManager prof = ProfileManager.getInstance();
					try {
						prof.doUploadProfile(fbAccount.getText().toString(), 
												twAccount.getText().toString(),
												null, 
												skypeAccount.getText().toString(),
												lnAccount.getText().toString(),
												tags.getText().toString(), 
												_mImageBitmap);
						startActivity(new Intent(getApplicationContext(),NearMeActivity.class));
					} catch (IOException e) {
						showToast("Eccezione: " + e.getMessage(), Toast.LENGTH_LONG);
						return;
					}
					showToast("OK!", Toast.LENGTH_LONG);
				} else {
					showToast("You must take the photo", Toast.LENGTH_LONG); 
				}
				
			}
		});
		_photoBtn = (ImageButton) findViewById(R.id.profilePhoto); 
		_photoBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dispatchTakePictureIntent(1337);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==1337) {
			_hasMadePhoto = true;
			Bundle extras = data.getExtras();
			_mImageBitmap = (Bitmap) extras.get("data");
			if(_mImageBitmap != null) {
				_photoBtn.setImageBitmap(_mImageBitmap);
			}
		}
	} 

	private void dispatchTakePictureIntent(int actionCode) {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
		startActivityForResult(takePictureIntent, actionCode);
	}
	
	public void showToast(CharSequence text, int duration) {
		Context context = getApplicationContext();
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
