package it.enchante;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.googlecode.androidannotations.annotations.UiThread;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.util.Log;
import android.widget.ListAdapter;


public class ProfileManager {
	
	private String ENCHANTE_ROOT_URL = "http://virtual.carlodemicheli.com/faceit/";
	private String CREATE_USER = "create_user.php";
	private String ADD_PROFILE = "add_profile.php";
	private String ADD_LOCATION = "add_location.php";
	
	// HTTP parameters
	private String PARAM_NAME = "name";
	private String PARAM_SURNAME = "surname";
	private String PARAM_EMAIL = "email";
	private String PARAM_TOKEN = "token";
	private String PARAM_FILE = "file";
	private String PARAM_ACCOUNTS = "accounts";
	private String PARAM_APMAC = "ap_mac";

	// JSON parameters
	private String PARAM_PROFILE_TYPE = "profile_type";
	private String PARAM_ACCOUNT_TYPE = "account_type";
	private String PARAM_ACCOUNT_INFO = "account_info";
	private String PARAM_MAC = "mac";
	
    private static byte[] sBuffer = new byte[512];
    
    public String TOKEN = null;
    
    private static ProfileManager instance = null;
    private LazyAdapter adapter = null;
    
	public static String UserName;
	public static String UserSurname;
	public static String UserEmail;
	
	public static ArrayList<Contact> contacts;
    
    public static synchronized ProfileManager getInstance() {
        if (instance == null)
        	instance = new ProfileManager();
        return instance;
    }
      
    public void setAdapter(LazyAdapter adapter) {
    	this.adapter = adapter;
    }
    
    public boolean doCreateUser(String name, String surname, String email) throws IOException, ParseException {
    	
    	String createUserUrl = ENCHANTE_ROOT_URL + CREATE_USER;
//    								PARAM_NAME + "=" + name + "&" + 
//    								PARAM_SURNAME + "=" + surname + "&" + 
//    								PARAM_EMAIL + "=" + email;
    	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair(PARAM_NAME, name));
    	params.add(new BasicNameValuePair(PARAM_SURNAME, surname));
    	params.add(new BasicNameValuePair(PARAM_EMAIL, email));
    	
    	String resp = doPostHttpRequest(createUserUrl, params);
    	
    	JSONParser parser = new JSONParser();
    	Object obj = parser.parse(resp);
    	if (obj == null){
    		Log.v("CREATE USER", "Parsed JSON object was null!");
    		return false;
    	}
    	
    	JSONObject jsonResp = (JSONObject) obj;
    	TOKEN = (String) jsonResp.get("token");
    	Log.i("CREATE USER", "Got token: " + TOKEN);
    	File someFile = new File(Environment.getExternalStorageDirectory(),"TokenSegreto.txt");
    	FileOutputStream fos = new FileOutputStream(someFile);
    	fos.write( TOKEN.getBytes() );
    	fos.flush();
    	fos.close();

    	if (TOKEN == null && "".equalsIgnoreCase(TOKEN))
    		return false;
    	
    	return true;    	
    }
    
    public boolean doUploadProfile(String facebook, String twitter, String cell, String skype,
    								String linkedin, String interest, Bitmap bmPhoto) throws IOException {
    	
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	bmPhoto.compress(CompressFormat.JPEG, 75, bos);
		byte[] data = bos.toByteArray();
		
		String urlUploadPhoto = ENCHANTE_ROOT_URL + ADD_PROFILE;

		JSONObject fb = new JSONObject();
		fb.put(PARAM_PROFILE_TYPE, "1");
		fb.put(PARAM_ACCOUNT_TYPE, "1");
		fb.put(PARAM_ACCOUNT_INFO, facebook);
		JSONObject tw = new JSONObject();
		tw.put(PARAM_PROFILE_TYPE, "1");
		tw.put(PARAM_ACCOUNT_TYPE, "2");
		tw.put(PARAM_ACCOUNT_INFO, twitter);
		JSONObject cel = new JSONObject();
		cel.put(PARAM_PROFILE_TYPE, "1");
		cel.put(PARAM_ACCOUNT_TYPE, "3");
		cel.put(PARAM_ACCOUNT_INFO, cell);
		JSONObject skyp = new JSONObject();
		skyp.put(PARAM_PROFILE_TYPE, "1");
		skyp.put(PARAM_ACCOUNT_TYPE, "4");
		skyp.put(PARAM_ACCOUNT_INFO, skype);
		JSONObject lin = new JSONObject();
		lin.put(PARAM_PROFILE_TYPE, "1");
		lin.put(PARAM_ACCOUNT_TYPE, "5");
		lin.put(PARAM_ACCOUNT_INFO, linkedin);
		JSONObject intrst = new JSONObject();
		intrst.put(PARAM_PROFILE_TYPE, "1");
		intrst.put(PARAM_ACCOUNT_TYPE, "6");
		intrst.put(PARAM_ACCOUNT_INFO, interest);
		
		org.json.simple.JSONArray detArray = new org.json.simple.JSONArray();
		detArray.add(fb);
		detArray.add(tw);
		detArray.add(cel);
		detArray.add(skyp);
		detArray.add(lin);
		detArray.add(intrst);
		
		StringWriter out = new StringWriter();
		JSONValue.writeJSONString(detArray, out);
		String jsonText = out.toString();
		Log.i("Upload Profile", jsonText);
		Log.i("Upload Profile", TOKEN);

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair(PARAM_TOKEN, TOKEN));
    	params.add(new BasicNameValuePair(PARAM_FILE, null));
    	params.add(new BasicNameValuePair(PARAM_ACCOUNTS, jsonText));		
		
		String resp = doPostMultipartEntity(urlUploadPhoto, params, data);
		
		return true;
    }
    
    @UiThread
    public boolean doSendWifiData(List<String> macs) throws IOException, ParseException {
    	
    	String addLocationUrl = ENCHANTE_ROOT_URL + ADD_LOCATION;
    	
    	org.json.simple.JSONArray macArray = new org.json.simple.JSONArray();
    	JSONObject obj;
    	for (String mac : macs){
    		obj = new JSONObject();
    		obj.put(PARAM_MAC, mac);
    		macArray.add(obj);
    	}
    	
    	StringWriter out = new StringWriter();
		JSONValue.writeJSONString(macArray, out);
		String jsonText = out.toString();
    	
    	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
    	params.add(new BasicNameValuePair(PARAM_TOKEN, TOKEN));
    	params.add(new BasicNameValuePair(PARAM_APMAC, jsonText));
    	
    	String resp = doPostHttpRequest(addLocationUrl, params);
    	
    	Log.i("doSendWifiData", resp);
    	
    	JSONParser parser = new JSONParser();
    	Object ob = parser.parse(resp);
    	if (ob == null) {
    		Log.v("doSendWifiData", "Parsed JSON object was null!");
    		return false;
    	}
    	
    	contacts = new ArrayList<Contact>();
    	
    	JSONArray array = (JSONArray) ob;
    	for (Object co: array) {
    		JSONObject c = (JSONObject) co;
    		Contact contact = new Contact();
    		contact.setUserId((String) c.get("user_id"));
    		
//    		JSONObject jo = (JSONObject) parser.parse((String) c.get("details"));
    		contact.setName((String) c.get("name"));
    		contact.setSurname((String) c.get("surname"));
    		contact.setPictureUrl(ENCHANTE_ROOT_URL + ((String) c.get("picture")));
    		
    		ArrayList<Account> accounts = new ArrayList<Account>();
    		JSONArray joarray = (JSONArray) c.get("accounts");
    		for (Object oaa: joarray) {
    			Account account = new Account();
    			JSONObject joa = (JSONObject) oaa;
    			account.setAccountInfo((String) joa.get("account_type"));
    			account.setAccountType((String) joa.get("account_info"));
	    		accounts.add(account);
    		}
    		contact.setAccounts(accounts);
    		
    		contacts.add(contact);
    	}
    	if(adapter!=null) {
    		adapter.notifyDataSetChanged();
    	}
    	return true;
    }
    
    
    // ==================================================================================================================
    // 										HTTP UTIL FUNCTIONS
    // ==================================================================================================================
    
    private String doPostMultipartEntity(String url, List<NameValuePair> params, byte[] photo) throws IOException {
    	
    	// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httpPost = new HttpPost(url);

	    try {
	    	MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	    	File someFile=null;
	    	for (int index=0; index < params.size(); index++) {
	            if (params.get(index).getName().equalsIgnoreCase(PARAM_FILE)) {
	                // If the key equals to "image", we use FileBody to transfer the data
//	            	entity.addPart(PARAM_FILE, new StringBody(new String(photo)));
	            	someFile = new File(Environment.getExternalStorageDirectory(),"blah");
	            	FileOutputStream fos = new FileOutputStream(someFile);
	            	fos.write( photo);
	            	fos.flush();
	            	fos.close();
	            	entity.addPart(PARAM_FILE, new FileBody(someFile, "image/jpg"));
	            } else {
	                // Normal string data
	                entity.addPart(params.get(index).getName(), new StringBody(params.get(index).getValue()));
	            }
	        }
	    	httpPost.setEntity(entity);

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httpPost);
	        
            // Check if server response is valid
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new ClientProtocolException("Invalid response from server: " +
                        								status.toString());
            }

	        // Pull content stream from response
            HttpEntity respEntity = response.getEntity();
            InputStream inputStream = respEntity.getContent();

            ByteArrayOutputStream content = new ByteArrayOutputStream();

            // Read response into a buffered stream
            int readBytes = 0;
            while ((readBytes = inputStream.read(sBuffer)) != -1) {
                content.write(sBuffer, 0, readBytes);
            }
            if(someFile!=null)
            	someFile.delete();
            // Return result from buffered stream
            return new String(content.toByteArray());
	        
	    } catch (ClientProtocolException e) {
	        throw e;
	    } catch (IOException e) {
	        throw e;
	    }
    }
    
    
    private String doPostHttpRequest(String url, List<NameValuePair> params) throws IOException {
		
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httpPost = new HttpPost(url);

	    try {
	    	httpPost.setEntity(new UrlEncodedFormEntity(params));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httpPost);
	        
            // Check if server response is valid
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new ClientProtocolException("Invalid response from server: " +
                        								status.toString());
            }

	        // Pull content stream from response
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();

            ByteArrayOutputStream content = new ByteArrayOutputStream();

            // Read response into a buffered stream
            int readBytes = 0;
            while ((readBytes = inputStream.read(sBuffer)) != -1) {
                content.write(sBuffer, 0, readBytes);
            }

            // Return result from buffered stream
            return new String(content.toByteArray());
	        
	    } catch (ClientProtocolException e) {
	        throw e;
	    } catch (IOException e) {
	        throw e;
	    }
    }
	

	private String doGetHttpRequest(String url) throws IOException {
		
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpGet httpGet = new HttpGet(url);

	    try {
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httpGet);
	        
            // Check if server response is valid
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new ClientProtocolException("Invalid response from server: " +
                        status.toString());
            }

	        
	        // Pull content stream from response
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
 
            ByteArrayOutputStream content = new ByteArrayOutputStream();

            // Read response into a buffered stream
            int readBytes = 0;
            while ((readBytes = inputStream.read(sBuffer)) != -1) {
                content.write(sBuffer, 0, readBytes);
            }

            // Return result from buffered stream
            return new String(content.toByteArray());
	        
	    } catch (ClientProtocolException e) {
	        throw e;
	    } catch (IOException e) {
	        throw e;
	    } 
	}
}
