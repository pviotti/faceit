package it.enchante;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.googlecode.androidannotations.annotations.UiThread;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.view.Window;
import android.database.DataSetObserver;

public class NearMeActivity  extends Activity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);	
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.near_me);
			
		  ProfileManager prof = ProfileManager.getInstance();
		  final ListView lv = (ListView)findViewById(R.id.listview_nearme);
		  LazyAdapter adapter=new LazyAdapter(this, prof.contacts);
		  adapter.registerDataSetObserver(new DataSetObserver() {
			  @UiThread
			public void onChanged() {
				// TODO Auto-generated method stub
				  lv.invalidateViews();
			  }
		  });
		  prof.setAdapter(adapter);
		  lv.setAdapter(adapter);
		  lv.setTextFilterEnabled(true);

		  /*lv.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View view, 
		        int position, long id) {
		      // When clicked, show a toast with the TextView text
		      Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
		          Toast.LENGTH_SHORT).show();
		    }
		  });*/
		}
}
