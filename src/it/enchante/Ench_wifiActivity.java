package it.enchante;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.parser.ParseException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

public class Ench_wifiActivity {

	WifiManager wifi;
	int size = 0;
	List<ScanResult> results;

	String ITEM_KEY = "key";
	ArrayList<HashMap<String, String>> arraylist = new ArrayList<HashMap<String, String>>();

	public void initWifiManager(Context context) {
		wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (wifi.isWifiEnabled() == false) {
			wifi.setWifiEnabled(true);
		}

		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context c, Intent intent) {
				results = wifi.getScanResults();
				size = results.size();
				Log.d("WifiScan",results.toString());
				
				ProfileManager prof = ProfileManager.getInstance();
				List<String> macs = new ArrayList<String>();
				for (ScanResult sr : results)
					macs.add(sr.BSSID);
				try {
					prof.doSendWifiData(macs);
				} catch (IOException e) {
					Log.e("doSendWifiData Exception", "Exception in doSendWifiData: " +e.getMessage());
				} catch (ParseException e) {
					Log.e("doSendWifiData Exception", "Exception in doSendWifiData: " +e.getMessage());
				}					
			}
		}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
	} 

	public void startScan() {
		arraylist.clear();
		wifi.startScan();
		try { 
			size = size - 1;
			while (size >= 0) {
				HashMap<String, String> item = new HashMap<String, String>();
				item.put(ITEM_KEY,
						results.get(size).SSID + "  "
								+ results.get(size).capabilities);
				arraylist.add(item);
				size--;
			}
		} catch (Exception e) {
			Log.e("WifiScan","error in scanning:"+e.getMessage());
		}
	}
}